<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>URL Shortener</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
            flex-direction: column;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
        form {
            margin-bottom: 20px;
        }
        .ok-result {
            color: #288009;
            font-weight: 700;
            margin-bottom: 20px;
        }
        .error-result {
            color: #ad232e;
            font-weight: 700;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if(isset($link) && $link)
        <div class="ok-result">
Your short link: {{route('hashLink', ['hash' => $link->hash])}}
        </div>
    @endif
    @if(isset($error) && $error)
        <div class="error-result">
 {{$error}}
        </div>
     @endif

    <div>
   <form method="post">
       @csrf
       url: <input type="text" name="url" placeholder="http://test.com">
       <input type="submit">
   </form>
    </div>
    <div>
    Last added:<br>
    @foreach($links as $link)
            <a href="{{route('hashLink', ['hash' => $link->hash])}}">{{$link->url}}</a><br>
    @endforeach
    </div>
</div>
</body>
</html>
