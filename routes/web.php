<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'LinksController@index');
Route::post('/', 'LinksController@addLink');

Route::get('/{hash}', 'LinksController@redirectTo')
    ->where('hash', '[0-9a-z]+')
    ->name('hashLink');