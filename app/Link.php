<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public $table = 'links';
    public $timestamps = false;
    protected $fillable  = ['url'];
    protected $appends = ['hash'];

    public function getHashAttribute()
    {
        return dechex($this->id);
    }

    public static function getByHash($hash)
    {
        return hexdec($hash);
    }
}
