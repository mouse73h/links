<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Link;
use Redirect;

class LinksController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function index()
    {
        $lastLinks = Link::orderBy('id', 'desc')->take(13)->get();
        return view('index', ['links' => $lastLinks ]);
    }

    public function addLink(Request $request)
    {
        $link = false;
        $error = false;

        $url = $request->get('url');
        if( filter_var($url, FILTER_VALIDATE_URL) ) {
            $link = Link::firstOrCreate(['url' => $url]);
        } else {
            $error = 'Bad link Format. Use http://...';
        }

        $lastLinks = Link::orderBy('id', 'desc')->take(13)->get();
        return view('index', ['error' => $error, 'link' => $link, 'links' => $lastLinks]);

    }

    public function redirectTo($hash)
    {
        $id = Link::getByHash($hash);
        $link = Link::find($id);
        if ($link) {
            return Redirect::away($link->url, 308);
        } else {
            abort(404);
        }
    }
}